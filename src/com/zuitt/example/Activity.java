package com.zuitt.example;

import java.util.Scanner;
public class Activity {

    public static void main(String[] args) {

        double firstSubject = 0, secondSubject = 0, thirdSubject =0, average = 0;
        String firstName, lastName;

        Scanner scanner = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = scanner.nextLine();

        System.out.println("Last Name:");
        lastName = scanner.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = scanner.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = scanner.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = scanner.nextDouble();

        average = (firstSubject+secondSubject+thirdSubject) / 3;

        // output in console
        System.out.println("Good day, " + firstName + " " + lastName+".");
        System.out.println("Your grade average is: " + average);

    }

}

